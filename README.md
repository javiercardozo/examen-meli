# MELI - Examen tecnico #

Proyecto spring-boot rest con base de datos mysql y hosteado en Heroku.

### Pre-requisitos ###

- Java 8 o superior.
- Maven 3.5 o superior.
- Eclipse u otro IDE.

### Instalación ###

- Clonar el repositorio ( git clone https://bitbucket.org/javiercardozo/examen-meli.git ).
- Importar proyecto maven desde el IDE.


### Pruebas ###

Documentación ( Swagger ) https://spring-boot-meli-heroku.herokuapp.com/swagger-ui.html
