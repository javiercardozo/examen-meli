package ar.com.cardozojavier.meli.validations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = IpValidador.class)
@Retention(RUNTIME)
@Target({ FIELD, METHOD })
public @interface Ip {

	String message() default "Formato de ip incorrecto";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };
	
}
