package ar.com.cardozojavier.meli.dao;

import java.util.List;

import ar.com.cardozojavier.meli.models.Pais;

public interface IPaisDao {

	public Pais findPaisByCodigo(String codigo);

	public void save(Pais pais);

	public void update(Pais pais);

	public List<Pais> findAll();
}
