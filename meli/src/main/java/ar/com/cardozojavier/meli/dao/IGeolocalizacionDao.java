package ar.com.cardozojavier.meli.dao;

public interface IGeolocalizacionDao {

	public String findCodigoByIp(String ip);
}
