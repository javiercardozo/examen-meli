package ar.com.cardozojavier.meli.dao.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import ar.com.cardozojavier.meli.dao.IGeolocalizacionDao;
import ar.com.cardozojavier.meli.models.InfoIp;

@Repository
public class GeolocalizacionDaoApi implements IGeolocalizacionDao {
	private static final Logger logger  = LoggerFactory.getLogger(GeolocalizacionDaoApi.class);
			
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${geolocalizacion.api.endpoint}")
	private String API_GEOLOZALIZACION_ENDPOINT;
	
	/**
	 * 
	 * Metodo encargado de ejecutar una api de geolocalizacion para obtener informacion
	 * de una ip.
	 * 
	 */
	@Override
	
	public String findCodigoByIp(String ip) {
		logger.info("METODO: findCodigoByIp(".concat(String.valueOf(ip)).concat(")"));
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("IP", ip);
		
		logger.info("GET: ".concat(API_GEOLOZALIZACION_ENDPOINT));
		logger.info("PARAMS: ".concat(String.valueOf(map)));
		
		InfoIp infoIp = restTemplate.getForObject(API_GEOLOZALIZACION_ENDPOINT.concat("?{IP}"), InfoIp.class, map);
		
		logger.info("RESPONSE: ".concat(String.valueOf(infoIp)));
		
		return infoIp.getCodigoPais();
	}

}
