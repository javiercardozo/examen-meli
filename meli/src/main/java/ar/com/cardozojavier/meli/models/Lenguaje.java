package ar.com.cardozojavier.meli.models;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Lenguaje {

	private String codigo;
	private String descripcion;
	
	public Lenguaje() {
		
		this.codigo = "";
		this.descripcion = "";
	}
	
	public Lenguaje(String codigo, String descripcion) {
		
		this.codigo = codigo;
		this.descripcion = descripcion;
	}
	@JsonProperty("iso639_1")
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	@JsonProperty("nativeName")
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();	
		builder.append(descripcion);
		builder.append("(");
		builder.append(codigo);
		builder.append(")");
		return builder.toString();
	}
	
	
}
