package ar.com.cardozojavier.meli.utils;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.jackson.JsonComponent;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import ar.com.cardozojavier.meli.models.Tarifario;

/**
 * Clase deserializadora para la clase Tarifario.
 * 
 * 
 * NOTA: El origen de esta clase es poder parsear las cotizaciones que devuelve
 * la api en base a una o varias divisas.
 * 
 * @author javier
 *
 */
@JsonComponent
public class TarifarioJsonDeserializer extends JsonDeserializer<Tarifario>{
	
	@Override
	public Tarifario deserialize(JsonParser json, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		
		JsonNode jsonNode = json.getCodec().readTree(json);
		
		Tarifario tarifario = new Tarifario();
		
		//Obtenemos la base de la cotizacion
		tarifario.setBase(jsonNode.get("base").asText());
		
		//Obtenemos las cotizaciones.
		Iterator<String> itrDivisas = jsonNode.get("rates").fieldNames();
		String divisa;
		Map<String, String> cotizaciones = new LinkedHashMap<String, String>();
		
		while(itrDivisas.hasNext()) {
			divisa = itrDivisas.next();
			
			cotizaciones.put(divisa, jsonNode.get("rates").get(divisa).asText());
			
		}
		
		tarifario.setCotizaciones(cotizaciones);

		return tarifario;
	}

}
