package ar.com.cardozojavier.meli.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Estadistica {

	private double min;
	private double max;
	private double promedio;
	private String unidad;
	
	public Estadistica() {
		
		this.min = 0.0;
		this.max = 0.0;
		this.promedio = 0.0;
		this.unidad = "Kms";
	}
	
	public Estadistica(double min, double max, double promedio, String unidad) {
	
		this.min = min;
		this.max = max;
		this.promedio = promedio;
		this.unidad = unidad;
	}
	
	@JsonProperty("distancia_min")
	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}
	@JsonProperty("distancia_max")
	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	@JsonProperty("distancia_promedio")
	public double getPromedio() {
		return promedio;
	}

	public void setPromedio(double promedio) {
		this.promedio = promedio;
	}
	
	@JsonProperty("unidad")
	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Estadistica [min=");
		builder.append(min);
		builder.append(", max=");
		builder.append(max);
		builder.append(", promedio=");
		builder.append(promedio);
		builder.append("]");
		return builder.toString();
	}

	
}
