package ar.com.cardozojavier.meli.dao.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import ar.com.cardozojavier.meli.dao.IPaisDao;
import ar.com.cardozojavier.meli.models.Pais;

@Repository
@Qualifier("PaisDaoApi")
public class PaisDaoApi implements IPaisDao {
	private static final Logger logger  = LoggerFactory.getLogger(PaisDaoApi.class);
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${pais.api.endpoint}")
	private String API_PAIS_ENDPOINT;
	
	/**
	 * 
	 * Metodo encargado de ejecutar una api para obtener informacion de un pais dado su codigo iso.
	 * 
	 */
	@Override
	public Pais findPaisByCodigo(String codigo) {
		logger.info("METODO: findPaisByCodigo(".concat(String.valueOf(codigo)).concat(")"));
		
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("ISO_CODE", codigo);
		
		logger.info("GET: ".concat(API_PAIS_ENDPOINT));
		logger.info("PARAMS: ".concat(String.valueOf(map)));
		
		Pais pais = restTemplate.getForObject(API_PAIS_ENDPOINT, Pais.class, map);
		
		logger.info("RESPONSE: ".concat(String.valueOf(pais)));
		
		return pais;
	}

	@Override
	public void save(Pais pais) {
		
	}

	@Override
	public void update(Pais pais) {
		
	}

	@Override
	public List<Pais>  findAll() {
		return null;
	}

}
