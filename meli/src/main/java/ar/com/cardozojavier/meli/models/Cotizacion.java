package ar.com.cardozojavier.meli.models;

import java.math.BigDecimal;

public class Cotizacion {
	
	private String base;
	private BigDecimal valor;
	
	public Cotizacion() {
		
		this.base = "";
		this.valor = new BigDecimal(0.0);
	}
	
	public Cotizacion(String base, BigDecimal valor) {
		
		this.base = base;
		this.valor = valor;
	}
	
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Cotizacion [base=");
		builder.append(base);
		builder.append(", valor=");
		builder.append(valor);
		builder.append("]");
		return builder.toString();
	}

	
}
