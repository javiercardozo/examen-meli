package ar.com.cardozojavier.meli.services;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import ar.com.cardozojavier.meli.models.Cotizacion;
import ar.com.cardozojavier.meli.models.Pais;

@Service
public class IpService {
	private static final Logger logger  = LoggerFactory.getLogger(IpService.class);
	
	@Autowired
	private GeolocalizacionService geolocalizacionService;
	
	@Autowired
	private PaisService paisService;
	
	@Autowired
	private CotizacionService cotizacionService;
	
	
	/**
	 * Metodo encargado de buscar informacion de un pais en base a una ip.
	 * 
	 * @param ip
	 * @return
	 */
	@Async("asyncExecutor")
	public CompletableFuture<Pais> findPaisByIp(String ip) {
		logger.info("METODO: findPaisByIp(".concat(String.valueOf(ip)).concat(")"));
		
		//Obtenemos el codigo iso del pais.
		String codigo = geolocalizacionService.findCodigoByIp(ip);
		
		//Obtenemos informacion del pais.
		Pais pais = paisService.findPaisByCodigo(codigo);
				
		//Buscamos la/s cotizacion/es.
		Map<String, Cotizacion> cotizaciones = cotizacionService.findCotizacionesByMonedas(pais.getMonedas());
		
		//Actualizamos el pais
		pais.setIp(ip);
		pais.actualizarCotizaciones(cotizaciones);
		
		return CompletableFuture.completedFuture(pais);
	}

	
}
