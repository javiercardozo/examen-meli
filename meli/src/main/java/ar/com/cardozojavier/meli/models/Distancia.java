package ar.com.cardozojavier.meli.models;

public class Distancia {

	private String unidad;
	private double valor;
	
	public Distancia() {
		
		this.unidad = "Kms";
		this.valor = 0;
	}
	
	public Distancia(String unidad, double valor) {
		
		this.unidad = unidad;
		this.valor = valor;
	}
	
	public String getUnidad() {
		return unidad;
	}
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Distancia [unidad=");
		builder.append(unidad);
		builder.append(", valor=");
		builder.append(valor);
		builder.append("]");
		return builder.toString();
	}

	
}
