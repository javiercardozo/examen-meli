package ar.com.cardozojavier.meli.services;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import ar.com.cardozojavier.meli.dao.IPaisDao;
import ar.com.cardozojavier.meli.models.Pais;

@Service
public class PaisService {
	private static final Logger logger  = LoggerFactory.getLogger(PaisService.class);
	
	@Autowired
	@Qualifier("PaisDaoApi")
	private IPaisDao paisDaoApi;
	
	@Autowired
	@Qualifier("PaisDaoMysql")
	private IPaisDao paisDaoMysql;
	
	/**
	 * Metodo encargado de obtener informacion de un pais en base a codigo iso de un pais.
	 * 
	 * @param codigo
	 * @return
	 */
	public Pais findPaisByCodigo(String codigo){
		
		logger.info("METODO: findPaisByCodigo(".concat(String.valueOf(codigo)).concat(")"));
		
		return paisDaoApi.findPaisByCodigo(codigo);
		
	}
	
	/*
	 * 
	 * Metodo encargado de registrar la actividad del pais.
	 * 
	 */
	@Async("asyncExecutor")
	public void registrarActividad(Pais pais) {
		logger.info("METODO: registrarActividad(".concat(String.valueOf(pais)).concat(")"));
		
		//Si no tenemos registros del pais, insertamos el pais.
		if(paisDaoMysql.findPaisByCodigo(pais.getCodigo()) == null) {
			paisDaoMysql.save(pais);	
		}else {//Caso contrario actualizamos las ocurrencias.
			paisDaoMysql.update(pais);
		}
	}
	
	/**
	 * Metodo encargado de obtener el listado de paises que consultaron los servicios.
	 * 
	 * @return
	 */
	@Async("asyncExecutor")
	public CompletableFuture<List<Pais>> findAll() {
		logger.info("METODO: findAll()");
		
		List<Pais> paises = paisDaoMysql.findAll();
		
		return CompletableFuture.completedFuture(paises);
	}
	
}
