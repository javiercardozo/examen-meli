package ar.com.cardozojavier.meli.controllers;

import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.cardozojavier.meli.models.Estadistica;
import ar.com.cardozojavier.meli.services.EstadisticaService;

/**
 * Controller encargado de obtener estadisticas.
 * 
 * @author javier
 *
 */
@RestController
@RequestMapping(value = "/api/v1")
public class EstadisticaController {
	private static final Logger logger  = LoggerFactory.getLogger(EstadisticaController.class);
	
	@Autowired
	private EstadisticaService estadisticaService;
	
	/**
	 * Metodo encargado de obtener estadisticas de utilizacion de servicios.
	 * 
	 * @return
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	@GetMapping(value = "/stats")
	public Estadistica stats() throws InterruptedException, ExecutionException {
		logger.info("METODO: stats()");
		
		return estadisticaService.find();

	}
	
}
