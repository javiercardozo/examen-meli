package ar.com.cardozojavier.meli.services;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.cardozojavier.meli.dao.IEstadisticaDao;
import ar.com.cardozojavier.meli.models.Estadistica;
import ar.com.cardozojavier.meli.models.Pais;


@Service
public class EstadisticaService {

	private static final Logger logger  = LoggerFactory.getLogger(EstadisticaService.class);
	
	@Autowired
	private IEstadisticaDao estadisticaDao;
	
	@Autowired
	private PaisService paisService;
	
	/**
	 * Metodo encargado de obtener estadisticas de utilizacion de servicio
	 * 
	 * Devuelve informacion:
	 * 
	 * -Distancia mas cercana a buenos aires que utilizo el serviio.
	 * -Distancia mas lejana a buenos aires que utilizo el serviio.
	 * -Distancia promedio a buenos aires.
	 * 
	 * @return Estadistica
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	public Estadistica find() throws InterruptedException, ExecutionException {
		logger.info("METODO: find()");
		
		//Obtenemos la distancia mas cercana a buenos aires
		CompletableFuture<Double> min = estadisticaDao.findMinDistancia();
		//Obtenemos la distancia mas lejana a buenos aires
		CompletableFuture<Double> max = estadisticaDao.findMaxDistancia();
		//Obtenemos la distancia promedio
		CompletableFuture<List<Pais>> paises = paisService.findAll();
		
		CompletableFuture.allOf(min, max, paises).join();
		
		Estadistica estadistica = new Estadistica();
		
		estadistica.setMin(min.get());
		estadistica.setMax(max.get());
		estadistica.setPromedio(this.calulcarPromedio(paises.get()));
		
		return estadistica;
	}

	private double calulcarPromedio(List<Pais> paises) {
		double promedio = 0.0;
		
		if(paises != null && !paises.isEmpty()) {
			Iterator<Pais> itr = paises.iterator();
			Pais pais = null;
			
			long totalOcurrencias = 0l;
			double sumatoria = 0.0;
			
			while(itr.hasNext()) {
				
				pais = itr.next();
				
				sumatoria = sumatoria + ( pais.getDistancia() * pais.getOcurrencias() );
				
				totalOcurrencias = totalOcurrencias + pais.getOcurrencias();
				
			}
			
			promedio = sumatoria / totalOcurrencias;
		}
		
		return promedio;
	}
}
