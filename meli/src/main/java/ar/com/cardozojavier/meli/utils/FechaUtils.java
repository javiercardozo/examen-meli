package ar.com.cardozojavier.meli.utils;

import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ar.com.cardozojavier.meli.models.ZonaHoraria;
/**
 * Clase de utileria para fechas y horas.
 * 
 * @author javier
 *
 */
public class FechaUtils {
	private static final Logger logger  = LoggerFactory.getLogger(FechaUtils.class);
	
	private static final String DEFAULT_TIME_ZONE = "+00:00";
	/**
	 * Metodo encargado de obtener la hora actual en base a un timezone.
	 * 
	 * @param timeZone
	 * @return
	 */
	public static ZonaHoraria parseFromTimeZone(String timeZone) {
		ZonaHoraria zonaHoraria = new ZonaHoraria();//Default
		try {
			//Obtenemos el codigo, ejemplo UTC
			String codigo = timeZone;
			//Obtenemos la zona horaria, ejemplo -03:00
			String val = timeZone.substring(3, timeZone.length());
			
			//Si solo viene informado UTC -> usamos la zona horaria default +00:00
			val = (val!=null && !val.isEmpty())?val:DEFAULT_TIME_ZONE;
			
			//Actualizamos valores.
			zonaHoraria.setCodigo(codigo);
			zonaHoraria.setHora(horaFromZonaHoraria(val));
		
		}catch(Exception e) {
			//Agregar al log
			logger.error("No es posible parsear la zona horaria.");
		}
		
		return zonaHoraria;
	}
	
	/**
	 * Metodo encargado de obtener la hora actual formateada HH:mm:nn en base 
	 * a una zona horaria ( ej: -03:00 ).
	 * 
	 * Fuente: https://www.baeldung.com/java-zone-offset
	 * 
	 * @param timeZone
	 * @return
	 */
	public static String horaFromZonaHoraria(String timeZone) {
		
		ZoneOffset zoneOffSet= ZoneOffset.of(timeZone);
		OffsetDateTime date = OffsetDateTime.now(zoneOffSet);
		
		return date.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
	}
	
	public static String hoy() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return formatter.format(new Date());
	}
}
