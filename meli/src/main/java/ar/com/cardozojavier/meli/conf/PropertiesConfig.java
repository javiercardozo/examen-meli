package ar.com.cardozojavier.meli.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({
    @PropertySource("classpath:geolocalizacion.properties"),
    @PropertySource("classpath:pais.properties"),
    @PropertySource("classpath:cotizacion.properties")
})
public class PropertiesConfig {

}
