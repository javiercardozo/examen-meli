package ar.com.cardozojavier.meli.controllers;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import ar.com.cardozojavier.meli.models.Error;

@RestControllerAdvice
public class ErrorHandleController extends ResponseEntityExceptionHandler {
	private static final Logger logger  = LoggerFactory.getLogger(IpController.class);
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		Iterator<FieldError> itr = ex.getBindingResult().getFieldErrors().iterator();
		FieldError item = null;
		Error error = new Error();
		
		error.setStatus(String.valueOf(status.value()));
		error.setPath(((ServletWebRequest)request).getRequest().getRequestURI().toString());
		
		while(itr.hasNext()) {
			item = itr.next();
			
			error.getErrors().add(item.getField() + ":" + item.getDefaultMessage());
			
		}
			
		return new ResponseEntity<Object>(error, headers, status);
	}
	
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		logger.error("handleExceptionInternal");
		
		Error error = new Error();
		
		error.setStatus(String.valueOf(status.value()));
		error.setPath(((ServletWebRequest)request).getRequest().getRequestURI().toString());
		error.getErrors().add(body.toString());
		
		return new ResponseEntity<Object>(error, headers, status);
	}
	
	@ExceptionHandler(value = { Exception.class })
	protected ResponseEntity<Object> handleException(RuntimeException ex, WebRequest request) {
		logger.error("handleException(".concat(String.valueOf(ex)).concat(")"));
		String bodyOfResponse = "Internal server error";
		return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
	
}
