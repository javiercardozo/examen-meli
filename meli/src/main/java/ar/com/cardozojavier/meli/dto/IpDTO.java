package ar.com.cardozojavier.meli.dto;

import ar.com.cardozojavier.meli.validations.Ip;

public class IpDto {

	@Ip
	private String ip;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IpDTO [ip=");
		builder.append(ip);
		builder.append("]");
		return builder.toString();
	}
	
}
