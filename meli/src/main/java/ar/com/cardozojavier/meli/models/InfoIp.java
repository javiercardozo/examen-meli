package ar.com.cardozojavier.meli.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InfoIp implements Serializable {

	private static final long serialVersionUID = 4096138332270819534L;
	private String codigoPais;
	private String nombrePais;
	
	@JsonProperty("countryCode")
	public String getCodigoPais() {
		return codigoPais;
	}
	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}
	@JsonProperty("countryName")
	public String getNombrePais() {
		return nombrePais;
	}
	public void setNombrePais(String nombrePais) {
		this.nombrePais = nombrePais;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InfoIp [codigoPais=");
		builder.append(codigoPais);
		builder.append(", nombrePais=");
		builder.append(nombrePais);
		builder.append("]");
		return builder.toString();
	}
}
