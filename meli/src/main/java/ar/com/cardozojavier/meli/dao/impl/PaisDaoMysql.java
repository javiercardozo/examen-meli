package ar.com.cardozojavier.meli.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ar.com.cardozojavier.meli.dao.IPaisDao;
import ar.com.cardozojavier.meli.models.Pais;

@Repository
@Qualifier("PaisDaoMysql")
public class PaisDaoMysql implements IPaisDao{
	private static final Logger logger  = LoggerFactory.getLogger(PaisDaoMysql.class);
	
	@PersistenceContext
	private EntityManager em;
	
	/**
	 * Metodo encargado de obtener datos de un pais en base
	 * al codigo iso.
	 * 
	 */
	@Override
	@Transactional(readOnly = true)
	public Pais findPaisByCodigo(String codigo) {
		logger.info("METODO: findPaisByCodigo(".concat(String.valueOf(codigo)).concat(")"));
		return em.find(Pais.class, codigo);
	}
	
	/**
	 * Metodo encargado de guardar datos del pais.
	 * 
	 */
	@Override
	@Transactional()
	public void save(Pais pais) {
		logger.info("METODO: save(".concat(String.valueOf(pais)).concat(")"));
		em.persist(pais);
	}

	/**
	 * Metodo encargado de actualizar las ocurrencias de un pais.
	 * 
	 */
	@Override
	@Transactional()
	public void update(Pais pais) {
		logger.info("METODO: update(".concat(String.valueOf(pais)).concat(")"));
		em.createQuery("update Pais set ocurrencias = ocurrencias + 1 where codigo = :codigo")
		  .setParameter("codigo", pais.getCodigo())
	      .executeUpdate();
	}
	
	/*
	 * Metodo encargado de listar todos los paises que se registraron.
	 * 
	 */
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<Pais> findAll() {
		
		return em.createQuery("from Pais").getResultList();
		
	}

}
