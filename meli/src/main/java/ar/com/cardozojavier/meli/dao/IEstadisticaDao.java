package ar.com.cardozojavier.meli.dao;

import java.util.concurrent.CompletableFuture;

public interface IEstadisticaDao{
	
	public CompletableFuture<Double> findMinDistancia() throws InterruptedException;

	public CompletableFuture<Double> findMaxDistancia() throws InterruptedException;

}
