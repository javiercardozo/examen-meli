package ar.com.cardozojavier.meli.dao.impl;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import ar.com.cardozojavier.meli.dao.ICotizacionDao;
import ar.com.cardozojavier.meli.models.Moneda;
import ar.com.cardozojavier.meli.models.Tarifario;

@Repository
public class CotizacionDaoApi implements ICotizacionDao {
	private static final Logger logger  = LoggerFactory.getLogger(CotizacionDaoApi.class);
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${cotizacion.api.endpoint}")
	private String API_COTIZACION_ENDPOINT;
	
	/*
	 * Metodo encargado de ejecutar una api para obtener las tarifas de las cotizaciones
	 * de las monedas.
	 * 
	 */
	@Override
	public Tarifario findTarifarioByMonedas(List<Moneda> monedas) {
		logger.info("METODO: findTarifarioByMonedas(".concat(String.valueOf(monedas)).concat(")"));
		
		//Armamos los parametros.
		StringBuffer sb = new StringBuffer();
		Iterator<Moneda> itr = monedas.iterator();
		
		while(itr.hasNext()) {
			sb.append(itr.next().getCodigo());
			if(itr.hasNext()) {
				sb.append(",");
			}
		}
		
		String parametros = sb.toString();
		
		logger.info("GET: ".concat(API_COTIZACION_ENDPOINT));
		logger.info("PARAMS: ".concat(parametros));
		
		Tarifario tarifario = restTemplate.getForObject(API_COTIZACION_ENDPOINT.concat(parametros), Tarifario.class);
		
		logger.info("RESPONSE: ".concat(String.valueOf(tarifario)));
		
		return tarifario;
	}

}
