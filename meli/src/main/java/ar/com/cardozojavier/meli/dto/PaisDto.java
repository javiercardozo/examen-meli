package ar.com.cardozojavier.meli.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import ar.com.cardozojavier.meli.models.Lenguaje;
import ar.com.cardozojavier.meli.models.Moneda;
import ar.com.cardozojavier.meli.models.Pais;
import ar.com.cardozojavier.meli.models.ZonaHoraria;
import ar.com.cardozojavier.meli.utils.DistanciaUtil;
import ar.com.cardozojavier.meli.utils.FechaUtils;

public class PaisDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private String ip;
	private String fechaProceso;
	private String nombre;
	private String codigo;
	private List<String> lenguajes;
	private List<String> monedas;
	private List<String> horarios;
	private String distancia;
	
	@JsonProperty("ip")
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	@JsonProperty("date")
	public String getFechaProceso() {
		return fechaProceso;
	}
	public void setFechaProceso(String fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	@JsonProperty("country")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@JsonProperty("iso_code")
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	@JsonProperty("languages")
	public List<String> getLenguajes() {
		return lenguajes;
	}
	public void setLenguajes(List<String> lenguajes) {
		this.lenguajes = lenguajes;
	}
	@JsonProperty("currency")
	public List<String> getMonedas() {
		return monedas;
	}
	public void setMonedas(List<String> monedas) {
		this.monedas = monedas;
	}
	@JsonProperty("times")
	public List<String> getHorarios() {
		return horarios;
	}
	public void setHorarios(List<String> horarios) {
		this.horarios = horarios;
	}
	@JsonProperty("estimated_distance")
	public String getDistancia() {
		return distancia;
	}
	public void setDistancia(String distancia) {
		this.distancia = distancia;
	}
	
	/**
	 * Metodo encargado de formatear el nombre del pais
	 * 
	 * Ej: España ( Spain )
	 * 
	 * @param pais
	 * @return
	 */
	public void setFormatCountry(Pais pais) {
		this.nombre = String.format("%s (%s)", pais.getNombreNativo(), pais.getNombre());
	}
	
	/**
	 * Metodo encargado de formatear el lenguajes
	 * 
	 * Ej: Español ( es ) 
	 * 
	 * @param lenguaje
	 * @return
	 */
	public void setFormatLanguage(List<Lenguaje> lenguaje) {
		this.lenguajes = new ArrayList<String>();
		Iterator<Lenguaje> itr = lenguaje.iterator();
		Lenguaje item;
    	while(itr.hasNext()) {
    		item = itr.next();
    		this.lenguajes.add(String.format("%s (%s)", item.getDescripcion(), item.getCodigo()));
    	}
	}
	
	/**
	 * Metodo encargado de formatear la moneda con la cotizacion
	 * 
	 * Ej: ARS ( 1 ARS = #### EUR ) 
	 * 
	 * @param monedas
	 */
	public void setFormatCurrency(List<Moneda> monedas) {
		this.monedas = new ArrayList<String>();
		Iterator<Moneda> itr = monedas.iterator();
		Moneda item;
    	while(itr.hasNext()) {
    		item = itr.next();
    		this.monedas.add(String.format("%s (1 %s = %s %s)", item.getCodigo(), item.getCodigo(), item.getCotizacion().getValor(), item.getCotizacion().getBase()));
    	}
		
	}
	/**
	 * Metodo encargado de formatear las zonas horarias a horas actuales.
	 * 
	 * Ej: Ej: 20:21:03 ( UTC ) 
	 * 
	 * @param zonasHorarias
	 * @return
	 */
	 public void setFormatTimes(List<String> zonasHorarias) {
		
		this.horarios = new ArrayList<String>();
		Iterator<String> itr = zonasHorarias.iterator();
		String item;
		ZonaHoraria zh = null;
    	
		while(itr.hasNext()) {
    		item = itr.next();
    		
    		zh = FechaUtils.parseFromTimeZone(item);
    		this.horarios.add(String.format("%s (%s)", zh.getHora(), zh.getCodigo()));
    	}
    	
	}
	
	/**
	 * Metodo encargado de formatear las coordenadas del pais a distancia en KM 
	 * (Distancia estimada que hay entre el pais y bs as).
	 * 
	 * Ej: 1443 kms
	 * 
	 * @param coordenadas
	 * @return
	 */
	public void setFormatDistance(List<String> coordenadas) {
		String lat = coordenadas.get(0);
		String lng = coordenadas.get(1);
		this.distancia = String.format("%s %s", DistanciaUtil.getDistanciaByLatAndLngKM(lat, lng), "kms");
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PaisDto [ip=");
		builder.append(ip);
		builder.append(", fechaProceso=");
		builder.append(fechaProceso);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", codigo=");
		builder.append(codigo);
		builder.append(", lenguajes=");
		builder.append(lenguajes);
		builder.append(", monedas=");
		builder.append(monedas);
		builder.append(", horarios=");
		builder.append(horarios);
		builder.append(", distancia=");
		builder.append(distancia);
		builder.append("]");
		return builder.toString();
	}
}
