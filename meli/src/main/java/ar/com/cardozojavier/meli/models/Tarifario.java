package ar.com.cardozojavier.meli.models;

import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ar.com.cardozojavier.meli.utils.TarifarioJsonDeserializer;

@JsonDeserialize( using = TarifarioJsonDeserializer.class)
public class Tarifario {

	private String base;
	private Map<String, String> cotizaciones;

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public Map<String, String> getCotizaciones() {
		return cotizaciones;
	}

	public void setCotizaciones(Map<String, String> cotizaciones) {
		this.cotizaciones = cotizaciones;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Tarifario [cotizaciones=");
		builder.append(cotizaciones);
		builder.append("]");
		return builder.toString();
	}
	
	
}
