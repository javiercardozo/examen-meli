package ar.com.cardozojavier.meli.services;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.cardozojavier.meli.dao.ICotizacionDao;
import ar.com.cardozojavier.meli.models.Cotizacion;
import ar.com.cardozojavier.meli.models.Moneda;
import ar.com.cardozojavier.meli.models.Tarifario;

@Service
public class CotizacionService {

	private static final Logger logger  = LoggerFactory.getLogger(CotizacionService.class);
	
	@Autowired
	private ICotizacionDao cotizacionDao;
	
	
	/**
	 * Metodo encargado de obtener las cotizaciones en base a una lista de monedas.
	 * 
	 * 
	 * @param monedas
	 * @return
	 */
	public Map<String, Cotizacion> findCotizacionesByMonedas(List<Moneda> monedas) {
		
		logger.info("METODO: findCotizacionesByMonedas(".concat(String.valueOf(monedas)).concat(")"));
		
		//Obtenemos la tabla de tarifas
		Tarifario tarifario = cotizacionDao.findTarifarioByMonedas(monedas);
		
		//Por cada moneda, actualizamos la cotizacion en base a la tabla de tarifas.
		Iterator<Moneda> itr = monedas.iterator();
		Moneda moneda = null;
		
		Cotizacion cotizacion = null;
		BigDecimal base = new BigDecimal(1);
		BigDecimal valor = null;
		
		String valorSinConvertir;
		
		Map<String, Cotizacion> cotizacionesMap = new LinkedHashMap<String, Cotizacion>();
		
		while(itr.hasNext()) {
			moneda = itr.next();
			
			cotizacion = new Cotizacion();
			
			//Inicializamos la cotizacion por moneda.
			cotizacionesMap.put(moneda.getCodigo(), cotizacion);
			
			//Recuperamos la cotizacion segun la moneda ( valor sin convertir ). 
			valorSinConvertir = tarifario.getCotizaciones().get(moneda.getCodigo());
			
			if(valorSinConvertir != null) {
				//Realizamos la conversion.
				valor = base.divide(new BigDecimal(valorSinConvertir), 4, RoundingMode.HALF_UP);
				
				//Actualizamos los datos de la cotizacion.
				cotizacion.setBase(tarifario.getBase());
				cotizacion.setValor(valor);
			}
			
		}
		
		return cotizacionesMap;

	}

}
