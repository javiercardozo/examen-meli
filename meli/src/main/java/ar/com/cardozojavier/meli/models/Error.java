package ar.com.cardozojavier.meli.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Error {

	private Date timestamp;
	private String status;
	private List<String> errors;
	
	private String path;
	
	public Error() {
		
		this.timestamp = new Date();
		this.status = "";
		this.errors = new ArrayList<String>();
		
		this.path = "";
	}

	public Error(Date timestamp, String status, List<String> errors, String path) {
		
		this.timestamp = timestamp;
		this.status = status;
		this.errors = errors;
		
		this.path = path;
	}
	
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Error [timestamp=");
		builder.append(timestamp);
		builder.append(", status=");
		builder.append(status);
		builder.append(", errors=");
		builder.append(errors);
		
		builder.append(", path=");
		builder.append(path);
		builder.append("]");
		return builder.toString();
	}
}
