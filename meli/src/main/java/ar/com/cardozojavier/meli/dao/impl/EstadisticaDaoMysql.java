package ar.com.cardozojavier.meli.dao.impl;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import ar.com.cardozojavier.meli.dao.IEstadisticaDao;

@Repository
public class EstadisticaDaoMysql implements IEstadisticaDao {

	private static final Logger logger  = LoggerFactory.getLogger(EstadisticaDaoMysql.class);
	
	@PersistenceContext
	private EntityManager em;
	
	/**
	 * Metodo encargado de obtener la minima distancia que hay entre bs as y el sitio que ejecuto
	 * algun servicio.
	 * @throws InterruptedException 
	 * 
	 */
	@Async("asyncExecutor")
	@SuppressWarnings("rawtypes")
	@Override
	public CompletableFuture<Double> findMinDistancia() throws InterruptedException {
		logger.info("METODO: findMinDistancia()");
		List result = em.createQuery("select min(distancia) from Pais").getResultList();
		
		logger.info("result = " + result);
		
		Double min = new Double(0.0);
		
		if(result.get(0)!=null) {
			min = Double.valueOf(String.valueOf(result.get(0)));
		}
		
		Thread.sleep(1000L);    //Intentional delay
		
		return CompletableFuture.completedFuture(min);
	}

	/**
	 * Metodo encargado de obtener la maxima distancia que hay entre bs as y el sitio que ejecuto
	 * algun servicio.
	 * @throws InterruptedException 
	 * 
	 */
	@Async("asyncExecutor")
	@SuppressWarnings("rawtypes")
	@Override
	public CompletableFuture<Double> findMaxDistancia() throws InterruptedException {
		logger.info("METODO: findMaxDistancia()");
		
		List result = em.createQuery("select max(distancia) from Pais").getResultList();
		
		Double max = new Double(0.0);
		
		if(result.get(0)!=null) {
			max = Double.valueOf(String.valueOf(result.get(0)));
		}
		Thread.sleep(1000L);    //Intentional delay
		return CompletableFuture.completedFuture(max);
	}

}
