package ar.com.cardozojavier.meli.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.StringUtils;

public class IpValidador implements ConstraintValidator<Ip, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		//Primero validamos que no sea null, vacio o que tenga espacios
		if(value != null && !StringUtils.hasText(value)) {
			return false;
		}
		
		//Segundo validamos el formato de la IP: Fuente https://stackoverflow.com/Questions/5667371/validate-ipv4-address-in-java
		String PATTERN = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";

	    return value.matches(PATTERN);
	    
	}

}
