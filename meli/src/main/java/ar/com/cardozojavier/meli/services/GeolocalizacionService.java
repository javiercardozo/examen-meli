package ar.com.cardozojavier.meli.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.cardozojavier.meli.dao.IGeolocalizacionDao;

@Service
public class GeolocalizacionService {
	private static final Logger logger  = LoggerFactory.getLogger(GeolocalizacionService.class);
	
	@Autowired
	private IGeolocalizacionDao geolocalizacionDao;
	
	/**
	 * Metodo encargado de obtener el codigo iso de un pais dado una ip.
	 * 
	 * @param ip
	 * @return
	 */
	public String findCodigoByIp(String ip) {
		logger.info("METODO: findCodigoByIp(".concat(String.valueOf(ip)).concat(")"));
		return geolocalizacionDao.findCodigoByIp(ip);
	}

}
