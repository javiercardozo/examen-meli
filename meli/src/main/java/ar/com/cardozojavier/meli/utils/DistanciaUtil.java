package ar.com.cardozojavier.meli.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Clase de utileria para distancias.
 * 
 * @author javier
 *
 */
public class DistanciaUtil {
	
	private static final double latitudBsAs = -34.61315;
	private static final double longitudBsAs = -58.37723;
	
	/**
	 * Metdo encargado de calcular la distancia en KM en base a latitudes y longitudes.
	 * 
	 * @param origenLat
	 * @param origenLng
	 * @param destinoLat
	 * @param destinoLng
	 * @return
	 */
	public static double getDistanciaByLatAndLngKM(double origenLat, double origenLng, double destinoLat, double destinoLng) {
		
		double radioTierra = 6371;//en kilómetros 
        double dLat = Math.toRadians(destinoLat - origenLat); 
        double dLng = Math.toRadians(destinoLng - origenLng); 
        
        double sindLat = Math.sin(dLat / 2); 
        double sindLng = Math.sin(dLng / 2); 
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) 
                * Math.cos(Math.toRadians(origenLat)) * Math.cos(Math.toRadians(destinoLat)); 
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1)); 
        double distancia = radioTierra * va2;
		 
		return Double.valueOf(String.valueOf(BigDecimal.valueOf(distancia).setScale(0,RoundingMode.HALF_UP))).doubleValue();
		
	}
	
	/**
	 * Metodo encargado de calcular la distancia que hay hasta Bs As dada unas coordenadas (latitud y longitud).
	 * 
	 * @param destinoLat
	 * @param destinoLng
	 * @return
	 */
	public static double getDistanciaByLatAndLngKM(double destinoLat, double destinoLng) {
		return getDistanciaByLatAndLngKM(latitudBsAs, longitudBsAs, destinoLat, destinoLng);
	}
	
	/**
	 * Metodo encargado de calcular la distancia que hay hasta Bs As dada unas coordenadas (latitud y longitud).
	 * 
	 * @param destinoLat
	 * @param destinoLng
	 * @return
	 */
	public static double getDistanciaByLatAndLngKM(String destinoLat, String destinoLng) {
		return getDistanciaByLatAndLngKM(latitudBsAs, longitudBsAs, Double.valueOf(destinoLat).doubleValue(), Double.valueOf(destinoLng).doubleValue());
	}
}
