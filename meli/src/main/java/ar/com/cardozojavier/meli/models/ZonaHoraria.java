package ar.com.cardozojavier.meli.models;

public class ZonaHoraria {

	private String codigo;
	private String hora;
	
	public ZonaHoraria() {
		
		this.codigo = "UTC";
		this.hora = "00:00";
	}
	
	public ZonaHoraria(String codigo, String hora) {
		
		this.codigo = codigo;
		this.hora = hora;
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ZonaHoraria [codigo=");
		builder.append(codigo);
		builder.append(", hora=");
		builder.append(hora);
		builder.append("]");
		return builder.toString();
	}
}
