package ar.com.cardozojavier.meli.dao;

import java.util.List;

import ar.com.cardozojavier.meli.models.Moneda;
import ar.com.cardozojavier.meli.models.Tarifario;

public interface ICotizacionDao {

	public Tarifario findTarifarioByMonedas(List<Moneda> monedas);

}
