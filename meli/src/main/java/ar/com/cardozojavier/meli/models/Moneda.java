package ar.com.cardozojavier.meli.models;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Moneda {

	private String codigo;
	
	private Cotizacion cotizacion;
	
	public Moneda() {
		
		this.codigo = "";
		this.cotizacion = new Cotizacion();
	}
	
	public Moneda(String codigo, Cotizacion cotizacion) {
	
		this.codigo = codigo;
		this.cotizacion = cotizacion;
	}
	@JsonProperty("code")
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Cotizacion getCotizacion() {
		return cotizacion;
	}
	public void setCotizacion(Cotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Moneda [codigo=");
		builder.append(codigo);
		builder.append(", cotizacion=");
		builder.append(cotizacion);
		builder.append("]");
		return builder.toString();
	}
}
