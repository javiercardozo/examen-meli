package ar.com.cardozojavier.meli.models;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

import ar.com.cardozojavier.meli.utils.DistanciaUtil;


@Entity
@Table(name = "paises")
public class Pais implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	private String codigo;
	@Transient
	private String ip;
	private String nombre;
	private String nombreNativo;
	@Transient
	private List<Lenguaje> lenguajes;
	@Transient
	private List<Moneda> monedas;
	@Transient
	private List<String> zonasHorarias;
	@Transient
	private List<String> coordenadas;
	
	private double distancia;

	private Long ocurrencias;
	
	@PrePersist
	public void prePersist() {
		distancia = DistanciaUtil.getDistanciaByLatAndLngKM(this.getCoordenadas().get(0), this.getCoordenadas().get(1));
		ocurrencias = 1L;
	}
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public void setLenguajes(List<Lenguaje> lenguajes) {
		this.lenguajes = lenguajes;
	}
	@JsonProperty("name")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@JsonProperty("nativeName")
	public String getNombreNativo() {
		return nombreNativo;
	}
	public void setNombreNativo(String nombreNativo) {
		this.nombreNativo = nombreNativo;
	}
	@JsonProperty("alpha2Code")
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	@JsonProperty("languages")
	public List<Lenguaje> getLenguajes() {
		return lenguajes;
	}
	public void setLenguaje(List<Lenguaje> lenguajes) {
		this.lenguajes = lenguajes;
	}
	@JsonProperty("currencies")
	public List<Moneda> getMonedas() {
		return monedas;
	}
	public void setMonedas(List<Moneda> monedas) {
		this.monedas = monedas;
	}
	
	@JsonProperty("timezones")
	public List<String> getZonasHorarias() {
		return zonasHorarias;
	}
	public void setZonasHorarias(List<String> zonasHorarias) {
		this.zonasHorarias = zonasHorarias;
	}
	
	@JsonProperty("latlng")
	public List<String> getCoordenadas() {
		return coordenadas;
	}
	
	public void setCoordenadas(List<String> coordenadas) {
		this.coordenadas = coordenadas;
	}
	
	public Long getOcurrencias() {
		return ocurrencias;
	}

	public double getDistancia() {
		
		return distancia;
		
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Pais [codigo=");
		builder.append(codigo);
		builder.append(", ip=");
		builder.append(ip);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", nombreNativo=");
		builder.append(nombreNativo);
		builder.append(", lenguajes=");
		builder.append(lenguajes);
		builder.append(", monedas=");
		builder.append(monedas);
		builder.append(", zonasHorarias=");
		builder.append(zonasHorarias);
		builder.append(", coordenadas=");
		builder.append(coordenadas);
		builder.append(", distancia=");
		builder.append(distancia);
		builder.append("]");
		return builder.toString();
	}

	public void actualizarCotizaciones(Map<String, Cotizacion> cotizaciones) {
		if(this.monedas != null) {
			Iterator<Moneda> itr = monedas.iterator();
			Moneda moneda = null;
			while(itr.hasNext()) {
				moneda = itr.next();
				moneda.setCotizacion(cotizaciones.get(moneda.getCodigo()));
			}
		}
	}
	
}
