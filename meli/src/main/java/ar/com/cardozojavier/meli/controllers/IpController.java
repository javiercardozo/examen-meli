package ar.com.cardozojavier.meli.controllers;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.cardozojavier.meli.dto.IpDto;
import ar.com.cardozojavier.meli.dto.PaisDto;
import ar.com.cardozojavier.meli.models.Pais;
import ar.com.cardozojavier.meli.services.IpService;
import ar.com.cardozojavier.meli.services.PaisService;
import ar.com.cardozojavier.meli.utils.FechaUtils;

/**
 * Controller encargado de obtener informacion de un pais en base a una ip.
 * 
 * @author javier
 *
 */
@RestController
@RequestMapping(value = "/api/v1")
public class IpController {
	private static final Logger logger  = LoggerFactory.getLogger(IpController.class);
	
	@Autowired
	private IpService ipService;
	
	@Autowired
	private PaisService paisService;
	
	@Autowired
    private ModelMapper modelMapper;
	
	/**
	 * Metodo encargado de obtener informacion del pais origen de una direccion ip.
	 * 
	 * @param ipDto
	 * @return
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	@PostMapping(value = "/trace")
	public PaisDto trace(@Valid @RequestBody IpDto ipDto) throws InterruptedException, ExecutionException {
		logger.info("Metodo trace(".concat(String.valueOf(ipDto)).concat(")"));
		
		CompletableFuture<Pais> pais = ipService.findPaisByIp(ipDto.getIp());
		
		CompletableFuture.allOf(pais).join();
		
		paisService.registrarActividad(pais.get());
		
		return this.paisToDto(pais.get());

	}

	private PaisDto paisToDto(Pais pais) {

		PaisDto paisDto = modelMapper.map(pais, PaisDto.class);
		paisDto.setFechaProceso(FechaUtils.hoy());
		paisDto.setFormatCountry(pais);
		paisDto.setFormatCurrency(pais.getMonedas());
		paisDto.setFormatDistance(pais.getCoordenadas());
		paisDto.setFormatLanguage(pais.getLenguajes());
		paisDto.setFormatTimes(pais.getZonasHorarias());
		
		return paisDto;
	}
}
