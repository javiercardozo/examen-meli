package ar.com.cardozojavier.meli;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import ar.com.cardozojavier.meli.models.Moneda;
import ar.com.cardozojavier.meli.models.Tarifario;

@SpringBootTest
class ApiCotizacionTest {
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${cotizacion.api.endpoint}")
	private String API_COTIZACIONS_ENDPOINT;
	
	@Test
	void apiCotizacionTest() {
		
		List<Moneda> monedas = new ArrayList<Moneda>();
		
		Moneda moneda = null;
		
		moneda = new Moneda();
		moneda.setCodigo("ARS");
		
		monedas.add(moneda);
		
		moneda = new Moneda();
		moneda.setCodigo("USD");
		
		monedas.add(moneda);
		
		System.out.println("Monedas inicial = " + monedas);
		
		
		StringBuffer sb = new StringBuffer();
		Iterator<Moneda> itr = monedas.iterator();
		
		while(itr.hasNext()) {
			sb.append(itr.next().getCodigo());
			if(itr.hasNext()) {
				sb.append(",");
			}	
		
		}
		
		String parametros = sb.toString();
		
		System.out.println("parametros = " + parametros);
		
		Tarifario json = restTemplate.getForObject(API_COTIZACIONS_ENDPOINT.concat(parametros), Tarifario.class);

		System.out.println("json = " + json);
		
		itr = monedas.iterator();
		BigDecimal cotizacion = null;
		BigDecimal base = new BigDecimal(1);
		BigDecimal valor = null;
		
		while(itr.hasNext()) {
			moneda = itr.next();
			
			cotizacion = new BigDecimal(json.getCotizaciones().get(moneda.getCodigo()));
		
			valor = base.divide(cotizacion, 4, RoundingMode.HALF_UP);
			
			System.out.println("valor = " + valor);
			
			moneda.getCotizacion().setBase(json.getBase());
			moneda.getCotizacion().setValor(valor);
		}
		
		System.out.println("Monedas actualizadas = " + monedas);
	}

}
