package ar.com.cardozojavier.meli;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StringUtils;

import ar.com.cardozojavier.meli.models.Lenguaje;

@SpringBootTest
class MeliApplicationTests {

	@Test
	void contextLoads() {
		
	}
	
	
	@Test
	void distance() {
		double lat_arg = -34.0, lng_arg = -64.0, lat_uy = -33.0 , lng_uy = -56.0;
		

		double radioTierra = 6371;//en kilómetros 
        double dLat = Math.toRadians(lat_uy - lat_arg); 
        double dLng = Math.toRadians(lng_uy - lng_arg); 
        
        double sindLat = Math.sin(dLat / 2); 
        double sindLng = Math.sin(dLng / 2); 
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) 
                * Math.cos(Math.toRadians(lat_arg)) * Math.cos(Math.toRadians(lat_uy)); 
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1)); 
        double distancia = radioTierra * va2;

		System.out.println("distancia = " + distancia);
        
	}
	@Test
	void timeZone() {
		
		ZoneOffset zoneOffSet= ZoneOffset.of("+00:00");
		OffsetDateTime date = OffsetDateTime.now(zoneOffSet);
		
		System.out.println(date);
		  
		System.out.println(date.format(DateTimeFormatter.ofPattern("HH:mm:ss")));
		
		
	}
	
	@Test
	void utils() {
		String timeZone = "UTC";
		String DEFAULT = "+00:00";
		
		if(timeZone != null && StringUtils.hasText(timeZone)) {
			String val = timeZone.substring(3, timeZone.length());
			
			System.out.println((val != null && !val.isEmpty())?val:DEFAULT);
			
		}
	}
	
	@Test
	void test() {
		List<Lenguaje> lenguajes = new ArrayList<Lenguaje>();
		
		Lenguaje item = new Lenguaje("AR", "Argentina");
		
		lenguajes.add(item);
		
		item = new Lenguaje("ES", "España");
		
		lenguajes.add(item);
		
		System.out.println(lenguajes.toString());
	}
}
