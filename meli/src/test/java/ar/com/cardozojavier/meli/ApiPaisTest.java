package ar.com.cardozojavier.meli;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import ar.com.cardozojavier.meli.models.Pais;

@SpringBootTest
class ApiPaisTest {

	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${pais.api.endpoint}")
	private String API_PAIS_ENDPOINT;
	
	
	
	@Test
	void apiPaisTest() {
		
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("ISO_CODE", "ar");
		
		Pais json = restTemplate.getForObject(API_PAIS_ENDPOINT, Pais.class, map);
		
		System.out.println("json = " + json);
	}

}
